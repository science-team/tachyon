Description: rationalization -- one development header file
 The tachyon library C header `tachyon.h' file declares the
 functions that are meant to be exported. Unfortunately, it
 includes three other C sub-headers, two of them having a
 generic name; what is not appropriate for a system wide setup.
 This patch icorporates this sub-headers into the master header
 `tachyon.h'. Afterwards, only one header, which is named according
 to the package, has to be set up; what is very wise for a system
 wide set up.
 This patch is transparent for the final developer, and it is also
 meant to be submitted to the upstream maintainer.
Origin: vendor, Debian
Author: Jerome Benoit <calculus@rezozer.net>
Last-Update: 2014-10-20

--- a/src/tachyon.h
+++ b/src/tachyon.h
@@ -1,8 +1,8 @@
 /*
- * tachyon.h - The declarations and prototypes needed so that 3rd party     
- *   driver code can run the raytracer.  Third party driver code should       
- *   only use the functions in this header file to interface with the 
- *   rendering engine.                                            
+ * tachyon.h - The declarations and prototypes needed so that 3rd party
+ *   driver code can run the raytracer.  Third party driver code should
+ *   only use the functions in this header file to interface with the
+ *   rendering engine.
  *
  * $Id: tachyon.h,v 1.121 2013/04/09 16:44:41 johns Exp $
  *
@@ -11,24 +11,168 @@
 #if !defined(TACHYON_H)
 #define TACHYON_H 1
 
+/*
+ * Tachyon version strings for feature detection and compatibility testing.
+ */
+#define TACHYON_VERSION_STRING      "0.99"    /**< string version info  */
+#define TACHYON_MAJOR_VERSION       0         /**< major version number */
+#define TACHYON_MINOR_VERSION       99        /**< minor version number */
+#define TACHYON_PATCH_VERSION       0         /**< patch version number */
+
+
 #ifdef  __cplusplus
 extern "C" {
 #endif
 
-#include "util.h"    /* rt_timer_xxx() and rt_rand() */
-#include "hash.h"    /* rt_hash_xxx() */
+/*#include "util.h"*/    /* rt_timer_xxx() and rt_rand() */
+/*
+ * util.h - This file contains defines for the timer functions...
+ *
+ *  $Id: util.h,v 1.23 2012/10/17 04:25:57 johns Exp $
+ */
+
+#if !defined(RT_UTIL_H)
+#define RT_UTIL_H 1
+
+#if !defined(USESINGLEFLT)
+#define ACOS(x)    acos(x)
+#define COS(x)     cos(x)
+#define EXP(x)     exp(x)
+#define FABS(x)    fabs(x)
+#define POW(x, y)  pow(x, y)
+#define SIN(x)     sin(x)
+#define SQRT(x)    sqrt(x)
+#else
+#define ACOS(x)    acosf(x)
+#define COS(x)     cosf(x)
+#define EXP(x)     expf(x)
+#define FABS(x)    fabsf(x)
+#define POW(x, y)  powf(x, y)
+#define SIN(x)     sinf(x)
+#define SQRT(x)    sqrtf(x)
+#endif
+
+typedef void * rt_timerhandle;           /* a timer handle */
+rt_timerhandle rt_timer_create(void);    /* create a timer (clears timer)  */
+void rt_timer_destroy(rt_timerhandle);   /* create a timer (clears timer)  */
+void rt_timer_start(rt_timerhandle);     /* start a timer  (clears timer)  */
+void rt_timer_stop(rt_timerhandle);      /* stop a timer                   */
+double rt_timer_time(rt_timerhandle);    /* report elapsed time in seconds */
+double rt_timer_timenow(rt_timerhandle); /* report elapsed time in seconds */
+
+#define RT_RAND_MAX 4294967296.0         /* Max random value from rt_rand  */
+unsigned int rt_rand(unsigned int *);    /* thread-safe 32-bit RNG         */
 
-/******************************************************************/
-/* Constants and types defined for use with the Tachyon API calls */
-/******************************************************************/
+/* select the RNG to use as the basis for all of the floating point work */
+#define RT_RNG_USE_KISS93               1
+
+#if defined(RT_RNG_USE_QUICK_AND_DIRTY)
+
+/* Quick and Dirty RNG */
+typedef struct {
+  unsigned int randval;
+} rng_urand_handle;
+#define RT_RNG_MAX 4294967296.0       /* max urand value: 2^32 */
+
+#elif defined(RT_RNG_USE_MERSENNE_TWISTER)
+
+/* Mersenne Twister */
+typedef struct {
+  int mti;                /* mti==N+1 means mt[N] is not initialized */
+  unsigned int mt[624];   /* N: the array for the state vector  */
+  unsigned int mag01[2];
+} rng_urand_handle;
+#define RT_RNG_MAX 4294967296.0       /* max urand value: 2^32 */
+
+#elif defined(RT_RNG_USE_KISS93)
+
+/* KISS93 */
+typedef struct {
+  unsigned int x;
+  unsigned int y;
+  unsigned int z;
+  unsigned int w;
+  unsigned int c;
+  unsigned int k;
+  unsigned int m;
+} rng_urand_handle;
+#define RT_RNG_MAX 4294967296.0       /* max urand value: 2^32 */
+
+#else
+
+/* KISS99 */
+typedef struct {
+  unsigned int x;
+  unsigned int y;
+  unsigned int z;
+  unsigned int c;
+} rng_urand_handle;
+#define RT_RNG_MAX 4294967296.0       /* max urand value: 2^32 */
+
+#endif
+
+void rng_urand_init(rng_urand_handle *rngh);
+void rng_urand_seed(rng_urand_handle *rngh, unsigned int seed);
+unsigned int rng_urand(rng_urand_handle *rngh);
+
+typedef rng_urand_handle rng_frand_handle;
+typedef rng_urand_handle rng_drand_handle;
+
+void rng_frand_init(rng_frand_handle *rngh);
+/* generates a random number on [0,1)-real-interval */
+float rng_frand(rng_frand_handle *rngh);
+void rng_frand_seed(rng_frand_handle *rngh, unsigned int seed);
+
+void rng_drand_init(rng_drand_handle *rngh);
+/* generates a random number on [0,1)-real-interval */
+double rng_drand(rng_frand_handle *rngh);
+void rng_drand_seed(rng_frand_handle *rngh, unsigned int seed);
 
+/* routine to help create seeds for parallel runs */
+unsigned int rng_seed_from_tid_nodeid(int tid, int node);
+
+void jitter_offset2f(unsigned int *pval, float *xy);
+void jitter_disc2f(unsigned int *pval, float *xy);
+void jitter_sphere3f(rng_frand_handle *rngh, float *dir);
+
+#endif /* RT_UTIL_H */
+
+/*#include "hash.h"*/    /* rt_hash_xxx() */
 /*
- * Tachyon version strings for feature detection and compatibility testing.
+ * hash.h - This file contains prototypes for hash table code etc.
+ *
+ * $Id: hash.h,v 1.4 2011/02/05 08:10:11 johns Exp $
  */
-#define TACHYON_VERSION_STRING      "0.99"    /**< string version info  */
-#define TACHYON_MAJOR_VERSION       0         /**< major version number */
-#define TACHYON_MINOR_VERSION       99        /**< minor version number */
-#define TACHYON_PATCH_VERSION       0         /**< patch version number */
+
+#ifndef HASH_H
+#define HASH_H
+
+typedef struct rt_hash_t {
+  struct hash_node_t **bucket;        /**< array of hash nodes */
+  int size;                           /**< size of the array */
+  int entries;                        /**< number of entries in table */
+  int downshift;                      /**< shift cound, used in hash function */
+  int mask;                           /**< used to select bits for hashing */
+} rt_hash_t;
+
+/**
+ * Return code when a hash key is not find, or there's no collision
+ * upon insertion.
+ */
+#define HASH_FAIL -1
+
+void rt_hash_init(rt_hash_t *, int);
+int rt_hash_lookup (rt_hash_t *, const char *);
+int rt_hash_insert (rt_hash_t *, const char *, int);
+int rt_hash_delete (rt_hash_t *, const char *);
+void rt_hash_destroy(rt_hash_t *);
+char *rt_hash_stats (rt_hash_t *);
+
+#endif /* HASH_H */
+
+/******************************************************************/
+/* Constants and types defined for use with the Tachyon API calls */
+/******************************************************************/
 
 /*
  * Build Tachyon and its interfaces using either double- or single-precision
@@ -738,8 +882,49 @@
  * Include now-deprecated Tachyon APIs, unless the user has told us not to
  */
 #if !defined(TACHYON_NO_DEPRECATED)
-#include "tachyon_dep.h"
-#endif
+/*#include "tachyon_dep.h"*/
+/*
+ * tachyon_dep.h - Deprecated Tachyon APIs that have been replaced by
+ *                 newer APIs or improved functionality.
+ *                 Existing applications should be updated to avoid using
+ *                 these APIs as they will be removed in a future version.
+ *
+ * $Id: tachyon_dep.h,v 1.2 2011/02/15 20:27:58 johns Exp $
+ *
+ */
+
+#if !defined(TACHYON_DEP_H)
+#define TACHYON_DEP_H 1
+
+/**
+ * Define a camera for a perspective projection, given the specified
+ * zoom factor, aspect ratio, antialiasing sample count,
+ * maximum ray recursion depth, and
+ * camera center, view direction, and up direction, in a left-handed
+ * coordinate system.
+ */
+void rt_camera_setup(SceneHandle, flt zoom, flt aspect,
+                     int alias, int maxdepth,
+                     apivector ctr, apivector viewdir, apivector updir);
+
+/**
+ * Defines a named 1-D, 2-D, or 3-D texture image with a
+ * 24-bit RGB image buffer, without any file references.
+ * This allows an application to send Tachyon images for texture mapping
+ * without having to touch the filesystem.
+ */
+void rt_define_image(const char *name, int xsize, int ysize, int zsize,
+                     unsigned char *rgb24data);
+
+/** Set parameters for sky sphere background texturing.  */
+void rt_background_sky_sphere(SceneHandle, apivector up,
+                              flt topval, flt botval,
+                              apicolor topcolor, apicolor botcolor);
+
+
+#endif /* TACHYON_DEP_H */
+
+#endif /* ! TACHYON_NO_DEPRECATED */
 
 
 /*
--- a/src/hash.h
+++ /dev/null
@@ -1,40 +0,0 @@
-/*
- * hash.h - This file contains prototypes for hash table code etc.
- *
- * $Id: hash.h,v 1.4 2011/02/05 08:10:11 johns Exp $
- */
-
-#ifndef HASH_H
-#define HASH_H
-
-
-#ifdef __cplusplus
-extern "C" {
-#endif
-
-typedef struct rt_hash_t {
-  struct hash_node_t **bucket;        /**< array of hash nodes */
-  int size;                           /**< size of the array */
-  int entries;                        /**< number of entries in table */
-  int downshift;                      /**< shift cound, used in hash function */
-  int mask;                           /**< used to select bits for hashing */
-} rt_hash_t;
-
-/** 
- * Return code when a hash key is not find, or there's no collision 
- * upon insertion.
- */
-#define HASH_FAIL -1
-
-void rt_hash_init(rt_hash_t *, int);
-int rt_hash_lookup (rt_hash_t *, const char *);
-int rt_hash_insert (rt_hash_t *, const char *, int);
-int rt_hash_delete (rt_hash_t *, const char *);
-void rt_hash_destroy(rt_hash_t *);
-char *rt_hash_stats (rt_hash_t *);
-
-#ifdef __cplusplus
-}
-#endif
-
-#endif
--- a/src/util.h
+++ /dev/null
@@ -1,111 +0,0 @@
-/* 
- * util.h - This file contains defines for the timer functions...
- *
- *  $Id: util.h,v 1.23 2012/10/17 04:25:57 johns Exp $
- */
-
-#if !defined(RT_UTIL_H) 
-#define RT_UTIL_H 1
-
-#if !defined(USESINGLEFLT)
-#define ACOS(x)    acos(x)
-#define COS(x)     cos(x)
-#define EXP(x)     exp(x)
-#define FABS(x)    fabs(x)
-#define POW(x, y)  pow(x, y)
-#define SIN(x)     sin(x)
-#define SQRT(x)    sqrt(x)
-#else
-#define ACOS(x)    acosf(x)
-#define COS(x)     cosf(x)
-#define EXP(x)     expf(x)
-#define FABS(x)    fabsf(x)
-#define POW(x, y)  powf(x, y)
-#define SIN(x)     sinf(x)
-#define SQRT(x)    sqrtf(x)
-#endif
-
-typedef void * rt_timerhandle;           /* a timer handle */
-rt_timerhandle rt_timer_create(void);    /* create a timer (clears timer)  */
-void rt_timer_destroy(rt_timerhandle);   /* create a timer (clears timer)  */
-void rt_timer_start(rt_timerhandle);     /* start a timer  (clears timer)  */
-void rt_timer_stop(rt_timerhandle);      /* stop a timer                   */
-double rt_timer_time(rt_timerhandle);    /* report elapsed time in seconds */
-double rt_timer_timenow(rt_timerhandle); /* report elapsed time in seconds */
-
-#define RT_RAND_MAX 4294967296.0         /* Max random value from rt_rand  */
-unsigned int rt_rand(unsigned int *);    /* thread-safe 32-bit RNG         */
-
-/* select the RNG to use as the basis for all of the floating point work */
-#define RT_RNG_USE_KISS93               1
-
-#if defined(RT_RNG_USE_QUICK_AND_DIRTY)
-
-/* Quick and Dirty RNG */
-typedef struct {
-  unsigned int randval;
-} rng_urand_handle;
-#define RT_RNG_MAX 4294967296.0       /* max urand value: 2^32 */
-
-#elif defined(RT_RNG_USE_MERSENNE_TWISTER)
-
-/* Mersenne Twister */
-typedef struct {
-  int mti;                /* mti==N+1 means mt[N] is not initialized */
-  unsigned int mt[624];   /* N: the array for the state vector  */
-  unsigned int mag01[2];
-} rng_urand_handle;
-#define RT_RNG_MAX 4294967296.0       /* max urand value: 2^32 */
-
-#elif defined(RT_RNG_USE_KISS93)
-
-/* KISS93 */
-typedef struct {
-  unsigned int x;
-  unsigned int y;
-  unsigned int z;
-  unsigned int w;
-  unsigned int c;
-  unsigned int k;
-  unsigned int m;
-} rng_urand_handle;
-#define RT_RNG_MAX 4294967296.0       /* max urand value: 2^32 */
-
-#else
-
-/* KISS99 */
-typedef struct {
-  unsigned int x;
-  unsigned int y;
-  unsigned int z;
-  unsigned int c;
-} rng_urand_handle;
-#define RT_RNG_MAX 4294967296.0       /* max urand value: 2^32 */
-
-#endif
-
-void rng_urand_init(rng_urand_handle *rngh);
-void rng_urand_seed(rng_urand_handle *rngh, unsigned int seed);
-unsigned int rng_urand(rng_urand_handle *rngh);
-
-typedef rng_urand_handle rng_frand_handle;
-typedef rng_urand_handle rng_drand_handle;
-
-void rng_frand_init(rng_frand_handle *rngh);
-/* generates a random number on [0,1)-real-interval */
-float rng_frand(rng_frand_handle *rngh);
-void rng_frand_seed(rng_frand_handle *rngh, unsigned int seed);
-
-void rng_drand_init(rng_drand_handle *rngh);
-/* generates a random number on [0,1)-real-interval */
-double rng_drand(rng_frand_handle *rngh);
-void rng_drand_seed(rng_frand_handle *rngh, unsigned int seed);
-
-/* routine to help create seeds for parallel runs */
-unsigned int rng_seed_from_tid_nodeid(int tid, int node);
-
-void jitter_offset2f(unsigned int *pval, float *xy);
-void jitter_disc2f(unsigned int *pval, float *xy);
-void jitter_sphere3f(rng_frand_handle *rngh, float *dir);
-
-#endif
--- a/src/tachyon_dep.h
+++ /dev/null
@@ -1,55 +0,0 @@
-/*
- * tachyon_dep.h - Deprecated Tachyon APIs that have been replaced by
- *                 newer APIs or improved functionality.
- *                 Existing applications should be updated to avoid using
- *                 these APIs as they will be removed in a future version.
- *
- * $Id: tachyon_dep.h,v 1.2 2011/02/15 20:27:58 johns Exp $
- *
- */
-
-
-#if !defined(TACHYON_NO_DEPRECATED)
-
-#if !defined(TACHYON_DEP_H)
-#define TACHYON_DEP_H 1
-
-#ifdef  __cplusplus
-extern "C" {
-#endif
-
-
-
-/**
- * Define a camera for a perspective projection, given the specified
- * zoom factor, aspect ratio, antialiasing sample count,
- * maximum ray recursion depth, and
- * camera center, view direction, and up direction, in a left-handed
- * coordinate system.
- */
-void rt_camera_setup(SceneHandle, flt zoom, flt aspect,
-                     int alias, int maxdepth,
-                     apivector ctr, apivector viewdir, apivector updir);
-
-/**
- * Defines a named 1-D, 2-D, or 3-D texture image with a
- * 24-bit RGB image buffer, without any file references.
- * This allows an application to send Tachyon images for texture mapping
- * without having to touch the filesystem.
- */
-void rt_define_image(const char *name, int xsize, int ysize, int zsize,
-                     unsigned char *rgb24data);
-
-/** Set parameters for sky sphere background texturing.  */
-void rt_background_sky_sphere(SceneHandle, apivector up,
-                              flt topval, flt botval,
-                              apicolor topcolor, apicolor botcolor);
-
-
-#ifdef  __cplusplus
-}
-#endif
-#endif
-
-#endif
-
--- a/src/hash.c
+++ b/src/hash.c
@@ -8,7 +8,7 @@
 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
-#include "hash.h"
+#include "tachyon.h"
 
 #define HASH_LIMIT 0.5
 
--- a/src/api.c
+++ b/src/api.c
@@ -35,7 +35,6 @@
 #include "vector.h"
 #include "intersect.h"
 #include "shade.h"
-#include "util.h"
 #include "imap.h"
 #include "global.h"
 #include "ui.h"
--- a/src/box.c
+++ b/src/box.c
@@ -15,7 +15,6 @@
 #include "box.h"
 #include "vector.h"
 #include "intersect.h"
-#include "util.h"
 
 int box_bbox(void * obj, vector * min, vector * max) {
   box * b = (box *) obj;
--- a/src/winbmp.c
+++ b/src/winbmp.c
@@ -12,7 +12,6 @@
 
 #define TACHYON_INTERNAL 1
 #include "tachyon.h"
-#include "util.h"
 #include "imageio.h" /* error codes etc */
 #include "winbmp.h"    /* the protos for this file */
 
--- a/src/vol.c
+++ b/src/vol.c
@@ -14,7 +14,6 @@
 #include "tachyon.h"
 #include "macros.h"
 #include "vector.h"
-#include "util.h"
 #include "parallel.h"
 #include "threads.h"
 #include "vol.h"
--- a/src/util.c
+++ b/src/util.c
@@ -16,7 +16,6 @@
 #define TACHYON_INTERNAL 1
 #include "tachyon.h"
 #include "macros.h"
-#include "util.h"
 #include "parallel.h"
 #include "ui.h"
 
--- a/src/ui.c
+++ b/src/ui.c
@@ -12,7 +12,6 @@
 #define TACHYON_INTERNAL 1
 #include "tachyon.h"
 #include "macros.h"
-#include "util.h"
 #include "ui.h"
 
 static void (* rt_static_ui_message) (int, char *) = NULL;
--- a/src/triangle.c
+++ b/src/triangle.c
@@ -14,7 +14,6 @@
 #include "vector.h"
 #include "macros.h"
 #include "intersect.h"
-#include "util.h"
 
 #define TRIANGLE_PRIVATE
 #include "triangle.h"
--- a/src/trace.c
+++ b/src/trace.c
@@ -16,7 +16,6 @@
 #include "vector.h"
 #include "shade.h"
 #include "camera.h"
-#include "util.h"
 #include "threads.h"
 #include "parallel.h"
 #include "intersect.h"
--- a/src/tgafile.c
+++ b/src/tgafile.c
@@ -11,7 +11,6 @@
 
 #define TACHYON_INTERNAL 1
 #include "tachyon.h"
-#include "util.h"
 #include "ui.h"
 #include "imageio.h"
 #include "tgafile.h"
--- a/src/texture.c
+++ b/src/texture.c
@@ -17,7 +17,6 @@
 #include "imap.h"
 #include "vector.h"
 #include "box.h"
-#include "util.h"
 
 static texture_methods normal_methods = {
   free
--- a/src/sphere.c
+++ b/src/sphere.c
@@ -14,7 +14,6 @@
 #include "macros.h"
 #include "vector.h"
 #include "intersect.h"
-#include "util.h"
 
 #define SPHERE_PRIVATE
 #include "sphere.h"
--- a/src/sgirgb.c
+++ b/src/sgirgb.c
@@ -11,7 +11,6 @@
 
 #define TACHYON_INTERNAL 1
 #include "tachyon.h"
-#include "util.h"
 #include "imageio.h" /* error codes etc */
 #include "sgirgb.h"
 
--- a/src/ring.c
+++ b/src/ring.c
@@ -14,7 +14,6 @@
 #include "macros.h"
 #include "vector.h"
 #include "intersect.h"
-#include "util.h"
 
 #define RING_PRIVATE
 #include "ring.h"
--- a/src/render.c
+++ b/src/render.c
@@ -17,7 +17,6 @@
 #include "imageio.h"
 #include "trace.h"
 #include "render.h"
-#include "util.h"
 #include "shade.h"
 #include "ui.h"
 #include "grid.h"
--- a/src/quadric.c
+++ b/src/quadric.c
@@ -15,7 +15,6 @@
 #include "quadric.h"
 #include "vector.h"
 #include "intersect.h"
-#include "util.h"
 
 int quadric_bbox(void * obj, vector * min, vector * max) {
   return 0;
--- a/src/psd.c
+++ b/src/psd.c
@@ -11,7 +11,6 @@
 
 #define TACHYON_INTERNAL 1
 #include "tachyon.h"
-#include "util.h"
 #include "imageio.h" /* error codes etc */
 #include "psd.h"
 
--- a/src/ppm.c
+++ b/src/ppm.c
@@ -14,7 +14,6 @@
 
 #define TACHYON_INTERNAL 1
 #include "tachyon.h"
-#include "util.h"
 #include "imageio.h" /* error codes etc */
 #include "ppm.h"
 
--- a/src/pngfile.c
+++ b/src/pngfile.c
@@ -17,7 +17,6 @@
 
 #define TACHYON_INTERNAL 1
 #include "tachyon.h"
-#include "util.h"
 #include "imageio.h" /* error codes etc */
 #include "pngfile.h" /* the protos for this file */
 
--- a/src/plane.c
+++ b/src/plane.c
@@ -14,7 +14,6 @@
 #include "macros.h"
 #include "vector.h"
 #include "intersect.h"
-#include "util.h"
 
 #define PLANE_PRIVATE
 #include "plane.h"
--- a/src/parvol.c
+++ b/src/parvol.c
@@ -13,7 +13,6 @@
 #include "tachyon.h"
 #include "macros.h"
 #include "vector.h"
-#include "util.h"
 #include "parallel.h"
 #include "box.h"
 #include "parvol.h"
--- a/src/parallel.c
+++ b/src/parallel.c
@@ -16,7 +16,6 @@
 #include "macros.h"
 #include "parallel.h"
 #include "tgafile.h"
-#include "util.h"
 #include "threads.h"
 
 #if !defined(_MSC_VER)
--- a/src/light.c
+++ b/src/light.c
@@ -14,7 +14,6 @@
 #include "macros.h"
 #include "vector.h"
 #include "intersect.h"
-#include "util.h"
 
 #define LIGHT_PRIVATE
 #include "light.h"
--- a/src/jpeg.c
+++ b/src/jpeg.c
@@ -16,7 +16,6 @@
 
 #define TACHYON_INTERNAL 1
 #include "tachyon.h"
-#include "util.h"
 #include "imageio.h" /* error codes etc */
 #include "jpeg.h"    /* the protos for this file */
 
--- a/src/imap.c
+++ b/src/imap.c
@@ -12,7 +12,6 @@
 #define TACHYON_INTERNAL 1
 #include "tachyon.h"
 #include "imap.h"
-#include "util.h"
 #include "parallel.h"
 #include "imageio.h"
 #include "ui.h"
--- a/src/imageio.c
+++ b/src/imageio.c
@@ -16,7 +16,6 @@
 #define TACHYON_INTERNAL 1
 #include "tachyon.h"
 #include "parallel.h"
-#include "util.h"
 #include "imageio.h"
 #include "ppm.h"     /* 24-bit and 48-bit PPM files */
 #include "psd.h"     /* 24-bit and 48-bit Photoshop files */
--- a/src/grid.c
+++ b/src/grid.c
@@ -15,7 +15,6 @@
 #include "macros.h"
 #include "vector.h"
 #include "intersect.h"
-#include "util.h"
 #include "ui.h"
 #include "parallel.h"
 
--- a/src/extvol.c
+++ b/src/extvol.c
@@ -13,7 +13,6 @@
 #include "tachyon.h"
 #include "macros.h"
 #include "vector.h"
-#include "util.h"
 #include "parallel.h"
 #include "threads.h"
 #include "box.h"
--- a/src/cylinder.c
+++ b/src/cylinder.c
@@ -14,7 +14,6 @@
 #include "macros.h"
 #include "vector.h"
 #include "intersect.h"
-#include "util.h"
 
 #define CYLINDER_PRIVATE 
 #include "cylinder.h"
--- a/src/camera.c
+++ b/src/camera.c
@@ -14,7 +14,6 @@
 #include "macros.h"
 #include "vector.h"
 #include "camera.h"
-#include "util.h"
 #include "intersect.h"
 
 /* 
