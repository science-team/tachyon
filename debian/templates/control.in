# Generated via `make -f debian/rules update-debian-control'.
# DO NOT MODIFY THIS FILE, instead
# modify debian/templates/control.in or the update-debian-control target machinery in debian/rules
# and regenerate it aferward.
Source: tachyon
Section: math
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 12),
 autoconf-archive, gnulib, libtool,
 pkg-config, help2man,
 mpi-default-dev,
 libopenmpi-dev [@ARCH_REVLISTOF_OPENMPI@],
 libmpich-dev,
 libjpeg-dev, libpng-dev,
 libgl-dev
Build-Conflicts:
 glx-diversions
Build-Depends-Indep:
 texlive-latex-base, texlive-latex-recommended, texlive-latex-extra,
 texlive-science, texlive-humanities,
 latex2html,
 rdfind, symlinks
Standards-Version: 4.7.0
Homepage: http://jedi.ks.uiuc.edu/~johns/raytracer/
Vcs-Git: https://salsa.debian.org/science-team/tachyon.git
Vcs-Browser: https://salsa.debian.org/science-team/tachyon

Package: libtachyon-serial-@LT_CURRENT@
Provides: libtachyon
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: tachyon-doc
Multi-Arch: same
Description: @SYNOPSIS_LIBRARY@ - runtime - serial flavour
 ##DescriptionMainPart##
 .
 This package provides the shared library required to run third party
 program compiled against the tachyon C library built for serial platforms.
 To compile your own programs you also need to install the concomitant
 libtachyon-serial-@LT_CURRENT@-dev package.

Package: libtachyon-serial-@LT_CURRENT@-dev
Provides: libtachyon-dev
Section: libdevel
Architecture: any
Depends: libtachyon-dev-common (= ${source:Version}), libtachyon-serial-@LT_CURRENT@ (= ${binary:Version}), ${misc:Depends}
Multi-Arch: same
Description: @SYNOPSIS_LIBRARY@ - development - serial flavour
 ##DescriptionMainPart##
 .
 This package contains the static libraries and symbolic links that
 third party developers using the tachyon C library built for serial
 platforms.

Package: libtachyon-mt-@LT_CURRENT@
Provides: libtachyon
Replaces: libtachyon-0.99
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: tachyon-doc
Multi-Arch: same
Description: @SYNOPSIS_LIBRARY@ - runtime - MT flavour
 ##DescriptionMainPart##
 .
 This package provides the shared library required to run third party
 program compiled against the tachyon C library built with multithreading.
 To compile your own programs you also need to install the concomitant
 libtachyon-mt-@LT_CURRENT@-dev package.

Package: libtachyon-mt-@LT_CURRENT@-dev
Provides: libtachyon-dev
Section: libdevel
Architecture: any
Depends: libtachyon-dev-common (= ${source:Version}), libtachyon-mt-@LT_CURRENT@ (= ${binary:Version}), ${misc:Depends}
Multi-Arch: same
Description: @SYNOPSIS_LIBRARY@ - development - MT flavour
 ##DescriptionMainPart##
 .
 This package contains the static libraries and symbolic links that
 third party developers using the tachyon C library built with
 multithreading will need.

Package: libtachyon-openmpi-@LT_CURRENT@
Provides: libtachyon
Section: libs
Architecture: @ARCH_REVLISTOF_OPENMPI@
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: tachyon-doc
Multi-Arch: same
Description: @SYNOPSIS_LIBRARY@ - runtime - OpenMPI flavour
 ##DescriptionMainPart##
 .
 This package provides the shared library required to run third party
 program compiled against the tachyon C library built against OpenMPI.
 To compile your own programs you also need to install the concomitant
 libtachyon-openmpi-@LT_CURRENT@-dev package.

Package: libtachyon-openmpi-@LT_CURRENT@-dev
Provides: libtachyon-dev
Section: libdevel
Architecture: @ARCH_REVLISTOF_OPENMPI@
Depends: libtachyon-dev-common (= ${source:Version}), libtachyon-openmpi-@LT_CURRENT@ (= ${binary:Version}), libopenmpi-dev, ${misc:Depends}
Multi-Arch: same
Description: @SYNOPSIS_LIBRARY@ - development - OpenMPI flavour
 ##DescriptionMainPart##
 .
 This package contains the static libraries and symbolic links that
 third party developers using the tachyon C library built against
 OpenMPI will need.

Package: libtachyon-mpich-@LT_CURRENT@
Provides: libtachyon
Section: libs
Architecture: @ARCH_REVLISTOF_MPICH@
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: tachyon-doc
Multi-Arch: same
Description: @SYNOPSIS_LIBRARY@ - runtime - MPICH flavour
 ##DescriptionMainPart##
 .
 This package provides the shared library required to run third party
 program compiled against the tachyon C library built against MPICH.
 To compile your own programs you also need to install the concomitant
 libtachyon-mpich-@LT_CURRENT@-dev package.

Package: libtachyon-mpich-@LT_CURRENT@-dev
Provides: libtachyon-dev
Section: libdevel
Architecture: @ARCH_REVLISTOF_MPICH@
Depends: libtachyon-dev-common (= ${source:Version}), libtachyon-mpich-@LT_CURRENT@ (= ${binary:Version}), libmpich-dev, ${misc:Depends}
Multi-Arch: same
Description: @SYNOPSIS_LIBRARY@ - development - MPICH flavour
 ##DescriptionMainPart##
 .
 This package contains the static libraries and symbolic links that
 third party developers using the tachyon C library built against
 MPICH will need.

Package: libtachyon-dev-common
Section: libdevel
Architecture: all
Depends: ${misc:Depends}
Suggests: libtachyon-dev
Multi-Arch: foreign
Description: @SYNOPSIS_LIBRARY@ - development - common material
 ##DescriptionMainPart##
 .
 This package contains the header files that third party developers
 using the tachyon C library will need.

Package: libtachyon-mpi-dev
Provides: libtachyon-dev
Section: libdevel
Architecture: any
Depends: ${@LIBTACHYON_MPI_DEFAULT_DEV@}, mpi-default-dev, ${misc:Depends}
Description: @SYNOPSIS_LIBRARY@ - development - default MPI flavour
 ##DescriptionMainPart##
 .
 This metapackage depends on the default MPI version of the tachyon
 C library development package for each architecture.

Package: tachyon
Architecture: all
Depends: tachyon-bin-nox | tachyon-bin, ${misc:Depends}
Multi-Arch: foreign
Description: @SYNOPSIS_TOOLS@ - metapackage
 ##DescriptionMainPart##
 .
 This metapackage allows multi-variant support for tools built upon tachyon.

Package: tachyon-bin-nox
Provides: tachyon-bin
Architecture: any
Depends: libtachyon-mt-@LT_CURRENT@ (= ${binary:Version}) | libtachyon, ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: foreign
Description: @SYNOPSIS_TOOLS@ - with no X support
 ##DescriptionMainPart##
 .
 This package provides a simple scene file parser front-end built upon
 tachyon but without X support.

Package: tachyon-bin-ogl
Provides: tachyon-bin
Architecture: any
Depends: libtachyon-mt-@LT_CURRENT@ (= ${binary:Version}) | libtachyon, ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: foreign
Description: @SYNOPSIS_TOOLS@ - with OpenGL display
 ##DescriptionMainPart##
 .
 This package provides a simple scene file parser front-end built upon
 tachyon and with OpenGL display.

Package: tachyon-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Enhances:
 libtachyon-dev, tachyon-bin,
 libtachyon-serial-@LT_CURRENT@, libtachyon-mt-@LT_CURRENT@,
 libtachyon-openmpi-@LT_CURRENT@, libtachyon-mpich-@LT_CURRENT@
Suggests: pdf-viewer
Multi-Arch: foreign
Description: @SYNOPSIS_MANUAL@ - reference manual
 ##DescriptionMainPart##
 .
 This package provides the reference manual for the tachyon C library.
 It also contains simple demo sources to build against the tachyon C library
 and scene material to parse with the simple tachyon scene parser front-end
 built upon the tachyon C library.
